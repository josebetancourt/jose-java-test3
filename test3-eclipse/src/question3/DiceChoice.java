package question3;

import javafx.event.Event;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DiceChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField money;
	private TextField bet;
	private Bet betOne;
	private String choice;
	
	
	DiceChoice(String choice, TextField message, TextField money, TextField bet, Bet betOne){
		this.message = message;
		this.money = money;
		this.bet = bet;
		this.betOne = betOne;
		this.choice = choice;
	}


	@Override
	public void handle(ActionEvent event) {
		String gameResult = this.betOne.finalMoney(this.choice, Integer.parseInt(this.bet.getText()));
		this.message.setText(gameResult);
		this.money.setText(Integer.toString(this.betOne.getMoneyUnits()));
		
	}
}
