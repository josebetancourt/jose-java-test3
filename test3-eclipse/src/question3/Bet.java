package question3;

import java.util.Random;

public class Bet {
	private int moneyUnits;
	Random rand = new Random();
//	private int moneyBet;
	
//	Bet(int moneyUnits, int moneyBet){
//		this.moneyUnits = moneyUnits;
////		this.moneyBet = moneyBet;
//	}
	
	public int getMoneyUnits() {
		return this.moneyUnits;
	}
	
//	public int getMoneyBet() {
//		return this.moneyBet;
//	}
	
	public String finalMoney(String choice, int moneyBet) {
		if (moneyBet > this.moneyUnits || this.moneyUnits == 0) {
			return "Invalid Bet";
		}
		
		
		int diceRoll = rand.nextInt(6) + 1;
		String evenOrOdd = "";
		
		if (diceRoll == 1 || diceRoll == 3 || diceRoll == 5) {
			evenOrOdd = "odd";
		}
		
		if (diceRoll == 2 || diceRoll == 4 || diceRoll == 6) {
			evenOrOdd = "even";
		}
		
		if (evenOrOdd.equals(choice)) {
			this.moneyUnits += moneyBet;
			return "You win";
		}
		else {
			this.moneyUnits -= moneyBet;
			return "You loose";
		}
		
	}
	
}
