package question3;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;



public class DiceRoll extends Application {
	private Bet betOne;
	
	public void start(Stage stage) {
		Group root = new Group();
		this.betOne = new Bet();
		

        Button even = new Button("play");
        Button odd = new Button("play");
        HBox buttons = new HBox();
        buttons.getChildren().addAll(even, odd);
        
		TextField message = new TextField("Roll!");
		TextField money = new TextField("250");
		TextField bet = new TextField("bet");
		HBox textFields = new HBox();
        textFields.getChildren().addAll(message, money, bet);
        
        VBox overall = new VBox();
        overall.getChildren().addAll(buttons, textFields);
        root.getChildren().add(overall);
        
        DiceChoice evenChoice = new DiceChoice("even", message, money, bet, this.betOne);
        even.setOnAction(evenChoice);
        DiceChoice oddChoice = new DiceChoice("odd", message, money, bet, this.betOne);
        odd.setOnAction(oddChoice);
        
        Scene scene = new Scene((Parent)root, 650.0, 300.0);
        scene.setFill((Paint)Color.BLUE);
        stage.setTitle("Die Roll Bet");
        stage.setScene(scene);
        stage.show();
	}
	
    public static void main(final String[] args) {
        Application.launch(args);
    }
}
