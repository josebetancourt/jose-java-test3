package question2;

public class Recursion {

	public static void main(String[] args) {
		int[] numbers = {20,21,22,23,24,25,30,18,19,6,7};
		int n = 3;
		
		//Only 23 and 25 match so it should print 2
		
		System.out.println(recursiveCount(numbers, n));
	}
	
	public static int recursiveCount(int[] numbers, int n) {
		int count = 0;
		
		// Check if the array is long enough to have an index of n
		if (numbers.length - 1 < n) {
			return count;
		}
		// Start checking from the n index if the number on n is > 20 and if its index is odd
		if (numbers[n] > 20 && n % 2 != 0) {
			count++; //one number is found in that index, add 1 to the count
			return count + recursiveCount(numbers, n + 1); //add 1 to n to move to the next index
		}
		else {
			//no number is found in that index, add 0 to the count, move to the next one
			return count + recursiveCount(numbers, n + 1);
		}
		
		
// 		Testing without recursion		
//		int count = 0;
//		
//		if (numbers.length == 0) {
//			return 0;
//		}
//		
//		else {
//			int[] oddArr = new int[numbers.length/2];
//			int j = 0;
//			
//			for(int i = 1; i < numbers.length; i++) {
//				if(i >= n) {
//					oddArr[j] = numbers[i];
//					j++;
//					i++;
//				}
//			}
//			for(int i = 0; i < oddArr.length; i++) {
//				if(oddArr[i] > 20) {
//					count++;
//				}
//			}
//		}		
//		return count;
		
	}

}
